from flask import Flask, request, render_template
from PIL import Image
from werkzeug.utils import secure_filename
from hashlib import md5

from os.path import join



app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = './static/media/'


@app.route("/", methods=['GET', 'POST'])
def home():
    return render_template("home1.html")


@app.route("/JPG-TO-PNG", methods=["GET", "POST"])
def jpg2png():
    if request.method == 'POST':
        file_obj = request.files['image_file']
        if file_obj.mimetype == "image/jpeg":
            filename = join(app.config["UPLOAD_FOLDER"], secure_filename(md5("sample".encode()).hexdigest()+'.png'))
            file_obj = Image.open(file_obj)
            file_obj.save(filename)
            return render_template("jpg_to_png.html", filename=filename)
        else:
            error = "Invalid Image Format"
            return render_template("jpg_to_png.html", error=error)

    return render_template("jpg_to_png.html")


@app.route("/PNG-TO-JPG", methods=["GET", "POST"])
def png2jpg():
    if request.method == "POST":
        file_obj = request.files['image_file']
        if file_obj.mimetype == "image/png":
            filename = join(app.config["UPLOAD_FOLDER"], secure_filename(md5("sample".encode()).hexdigest() + '.jpg'))
            file_obj = Image.open(file_obj)
            file_obj = file_obj.convert('RGB')
            file_obj.save(filename)
            return render_template("png_to_jpg.html", filename=filename)
        else:
            error = "Invalid Image Format"
            return render_template("png_to_jpg.html", error=error)

    return render_template("png_to_jpg.html")


@app.route("/JPG-TO-WEBP", methods=["GET", "POST"])
def jpg2webp():
    if request.method == "POST":
        file_obj = request.files['image_file']
        if file_obj.mimetype == "image/jpeg":
            filename = join(app.config["UPLOAD_FOLDER"], secure_filename(md5("sample".encode()).hexdigest()+'.webp'))
            file_obj = Image.open(file_obj)
            # file_obj = file_obj.convert('RGB')
            file_obj.save(filename)
            return render_template("jpg_to_webp.html", filename=filename)
        else:
            error = "Invalid Image Format"
            return render_template("jpg_to_webp.html", error=error)

    return render_template("jpg_to_webp.html")

@app.route("/WEBP-TO-JPG", methods=["GET", "POST"])
def webp2jpg():
    if request.method == "POST":
        file_obj = request.files['image_file']
        if file_obj.mimetype == "image/webp":
            filename = join(app.config["UPLOAD_FOLDER"], secure_filename(md5("sample".encode()).hexdigest()+'.jpeg'))
            file_obj = Image.open(file_obj)
            # file_obj = file_obj.convert('RGB')
            file_obj.save(filename)
            return render_template("webp_to_jpg.html", filename=filename)
        else:
            error = "Invalid Image Format"
            return render_template("webp_to_jpg.html", error=error)

    return render_template("webp_to_jpg.html")



@app.route("/PNG-TO-WEBP", methods=["GET", "POST"])
def png2webp():
    if request.method == "POST":
        file_obj = request.files['image_file']
        if file_obj.mimetype == "image/png":
            filename = join(app.config["UPLOAD_FOLDER"], secure_filename(md5("sample".encode()).hexdigest()+'.webp'))
            file_obj = Image.open(file_obj)
            # file_obj = file_obj.convert('RGB')
            file_obj.save(filename)
            return render_template("png_to_webp.html", filename=filename)
        else:
            error = "Invalid Image Format"
            return render_template("png_to_webp.html", error=error)

    return render_template("png_to_webp.html")

@app.route("/WEBP-TO-PNG", methods=["GET", "POST"])
def webp2png():
    if request.method == "POST":
        file_obj = request.files['image_file']
        if file_obj.mimetype == "image/webp":
            filename = join(app.config["UPLOAD_FOLDER"], secure_filename(md5("sample".encode()).hexdigest()+'.png'))
            file_obj = Image.open(file_obj)
            # file_obj = file_obj.convert('RGB')
            file_obj.save(filename)
            return render_template("webp_to_png.html", filename=filename)
        else:
            error = "Invalid Image Format"
            return render_template("webp_to_png.html", error=error)

    return render_template("webp_to_png.html")


@app.route("/ANY-TO-TIFF", methods=["GET", "POST"])
def any2tiff():
    if request.method == "POST":
        file_obj = request.files['image_file']
        if file_obj.mimetype == "image/*":
            filename = join(app.config["UPLOAD_FOLDER"], secure_filename(md5("sample".encode()).hexdigest()+'.tiff'))
            file_obj = Image.open(file_obj)
            # file_obj = file_obj.convert('RGB')
            file_obj.save(filename)
            return render_template("any_to_tiff.html", filename=filename)
        else:
            error = "Invalid Image Format"
            return render_template("any_to_tiff.html", error=error)

    return render_template("any_to_tiff.html")



if __name__ == "__main__":
    app.run(debug=True)
